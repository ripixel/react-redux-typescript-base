import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import Hello from '../components/Hello';
import store from '../store';
import { set as setConfig } from '../store/actions/config';

const rootElement = document.getElementById('root');
render(
	<Provider store={store}>
		<Hello />
	</Provider>,
	rootElement,
);

// This will change the values after 3 seconds
setTimeout(() => {
	store.dispatch(
		setConfig({
			framework: 'React',
			compiler: 'TypeScript',
		}),
	);
}, 3000);
