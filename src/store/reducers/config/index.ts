import { Reducer } from 'redux';
import { SET } from '../../actions/config';

export interface IState {
	thisShouldNotChange: string;
	framework: string;
	compiler: string;
}

const initialState: IState = {
	thisShouldNotChange: 'Hello!',
	framework: 'not yet set',
	compiler: 'not yet set',
};

export const reducer: Reducer<IState, any> = (state = initialState, action) => {
	switch (action.type) {
		case SET:
			return {
				...state,
				framework: action.payload.framework,
				compiler: action.payload.compiler,
			};
		default:
			return state;
	}
};

export default reducer;
