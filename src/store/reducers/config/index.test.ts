import { ISet, ISetPayload, SET } from '../../actions/config';
import { IState, reducer } from './index';

describe('store > reducers > config', () => {
	describe(SET + ' actions', () => {
		it('should correctly update state', () => {
			const inputActionPayload: ISetPayload = {
				framework: 'hello',
				compiler: 'dave',
			};
			const inputAction: ISet = {
				type: SET,
				payload: inputActionPayload,
			};
			const inputState: IState = {
				thisShouldNotChange: 'unchanged',
				framework: 'test',
				compiler: 'test2',
			};

			const expectedOutput: IState = {
				thisShouldNotChange: inputState.thisShouldNotChange,
				framework: inputActionPayload.framework,
				compiler: inputActionPayload.compiler,
			};

			const output: IState = reducer(inputState, inputAction);
			expect(output).toEqual(expectedOutput);
		});
	});
});
