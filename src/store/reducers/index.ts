import { combineReducers, Reducer } from 'redux';
import config, { IState as IConfigState } from './config';

export interface IState {
	config: IConfigState;
}

export const rootReducer: Reducer<IState> = combineReducers({
	config,
});

export default rootReducer;
