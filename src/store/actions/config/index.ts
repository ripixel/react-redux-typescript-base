export const SET = 'CONFIG_SET';

export interface ISetPayload {
	framework: string;
	compiler: string;
}

export interface ISet {
	type: typeof SET;
	payload: ISetPayload;
}
export const set = (config: ISetPayload): ISet => ({
	type: SET,
	payload: config,
});
