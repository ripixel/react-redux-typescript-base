import { ISet, ISetPayload, set, SET } from './index';

describe('store > actions > config', () => {
	describe('set', () => {
		it('should output the desired action', () => {
			const expectedPayload: ISetPayload = {
				framework: 'hello',
				compiler: 'dave',
			};
			const expectedOutput: ISet = {
				type: SET,
				payload: expectedPayload,
			};

			const output = set(expectedPayload);
			expect(output).toEqual(expectedOutput);
		});
	});
});
