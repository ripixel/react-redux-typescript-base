import { shallow } from 'enzyme';
import * as React from 'react';

import { Hello } from './index';

describe('components > Hello', () => {
	it('should render correctly', () => {
		const component = shallow(
			<Hello
				thisShouldNotChange='Hello'
				compiler='Jest'
				framework='React'
			/>,
		);

		expect(component).toMatchSnapshot();
	});
});
