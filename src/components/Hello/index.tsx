import * as React from 'react';
import { connect } from 'react-redux';
import { IState } from '../../store/reducers';

export interface IProps {
	thisShouldNotChange: string;
	compiler: string;
	framework: string;
}

export const Hello: React.SFC<IProps> = (props) => (
	<h1>
		{props.thisShouldNotChange} from {props.compiler} and {props.framework}!
	</h1>
);

const mapStateToProps = (state: IState) => ({
	thisShouldNotChange: state.config.thisShouldNotChange,
	framework: state.config.framework,
	compiler: state.config.compiler,
});

export default connect(mapStateToProps)(Hello);
