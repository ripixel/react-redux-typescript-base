# React Redux Typescript Base App

This is a very simple base implementation of React, Redux, and Typescript. No shenanigans or fancy-pants.

## Use

### Installation

```bash
npm install
```

That's it!

### Build

```bash
npm run build
```

Yup, that's it!

### Test

```bash
npm run test
```

Oh boi.

### Viewing

Open `public/index.html` in your favourite browser. Easy peasy.

## Development

The `src/app/index.tsx` is the entry point for the app. Do any setup in there (or separate it into a separate imported file).

Components should live in `src/components` (duh).

All redux-related activities should live within `src/store`. Initial implementation splits `actions` and `reducers`, with each-one being self-contained, and actions can be directly related (or not) to named reducers.

If you want middleware, they should go in `src/store/middleware`.

Tests (using Jest) are located in `*.test.*` format _next_ to the item they are testing - makes it nice and easy!

_Note about React and Redux Dev Tools in Chrome: You'll need to enable "Allow Access to File URLs" for the extensions!_
